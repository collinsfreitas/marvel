import React, { Component } from 'react';
import Header from './Header';
import Footer from './Footer';
import CharacterList from './CharecterList';

class App extends Component {
    constructor(props) {

        super(props);

        this.state = {
            item: [],
            isLoaded: false,
            search: '',
        }

        this.updateSearch = this.updateSearch.bind(this);

    }

    updateSearch (event) {
        this.setState({search: event.target.value})
    }

    render() {

        return (
            <div className="App">
                <Header></Header>
                <CharacterList></CharacterList>
                <Footer></Footer>
            </div>
        )

    }

}

export default App;
