import React, { Component } from 'react';
import logo from './assets/images/1200px-MarvelLogo.png';

class Header extends Component {

    render () {
        return (
            <header className="header">
                <div className="container">
                    <div className="row">
                        <div className="col">
                            <img src={logo} className="App-logo" alt="logo" />
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}

export default Header;