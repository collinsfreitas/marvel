import React, { Component } from 'react';

class Footer extends Component {

    render () {
        return (
            <footer className="footer">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            Candidato: <strong>Collins Freitas</strong>
                        </div>
                        <div className="col-12">
                            ©2018 MARVEL
                        </div>
                    </div>
                </div>
            </footer>
        )
    }
}

export default Footer;