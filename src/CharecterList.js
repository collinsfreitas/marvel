import React, { Component } from 'react';
import filter from './assets/images/filter.svg';

class CharacterList extends Component {

    constructor(props) {

        super(props);

        this.state = {
            todos: [],
            isLoaded: false,
            search: '',
            currentPage: 1,
            todosPerPage: 3
        }

        this.updateSearch = this.updateSearch.bind(this);
        this.handleClick = this.handleClick.bind(this);

    }

    handleClick(event) {
        event.preventDefault();
        this.setState({
          currentPage: Number(event.target.innerHTML)
        });

        this.componentDidMount();
      }

    componentDidMount () {
        fetch('https://gateway.marvel.com/v1/public/characters?ts=1&apikey=161a2b9a44ae809c662b3c8d9afb1cb7&hash=9a359111b94a1b6dbe0a9e918b0c1a96&offset=' + ((this.state.currentPage-1) * Number(this.state.limit)) + '&limit=20')
            .then(res => res.json())
            .then(json => {
                this.setState ({
                    isLoaded: true,
                    itemsTotal: json.data.total,
                    limit: json.data.limit,
                    items: json.data.results
                })
        });
    }

    updateSearch (event) {
        this.setState({search: event.target.value})
    }

    render() {

        var { isLoaded, items } = this.state;
        var page = 1;
        var pages = Math.ceil(this.state.itemsTotal / this.state.limit);
        var pagination = [];

        while(page <= pages)
        {
            pagination.push(<a className="page" onClick={this.handleClick} href="#">{page}</a>);
            page++;
        }

        if (!isLoaded) {
            return (
                <div className="container">
                    <div className="row">
                        <div className="col-12 text-center">
                            <div>Carregando...</div>
                        </div>
                    </div>
                </div>
            )
        }

        else {

            return (
                <div className="CharacterList">
                    <div></div>
                    <div className="container">
                        <div className="board">
                            <div className="row">
                                <div className="col-12">
                                    <h1 className="page-title">Character</h1>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-5">
                                    <input className="search-field" placeholder="Character..." type="text" value={this.state.search} onChange={this.updateSearch} />
                                </div>
                                <div className="col-6">
                                    <img src={filter} className="img-filter" alt="Filter" />
                                    <select className="select-a-z">
                                        <option value="">A</option>
                                        <option value="">Z</option>
                                    </select>
                                </div>
                            </div>
                            <div className="row">
                                {items.filter(
                                    items => items.name.toLowerCase().includes(this.state.search.toLowerCase())
                                ).map(item => (
                                    <div className="col-xs-12 col-sm-4 col-md-3 col-3-xl" key={item.id}>
                                        <div className="character">
                                            <img className="img-fluid picture" src={item.thumbnail.path + "." + item.thumbnail.extension} />
                                            <h3 className="name">{item.name}</h3>
                                            <div className="line"></div>
                                            <p className="description">{item.description ? item.description : 'Descrição não informada.'}</p>
                                        </div>
                                    </div>
                                ))}
                            </div>
                            <div className="row">
                                <div className="col-12">
                                    {pagination}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }

    }
}

export default CharacterList;